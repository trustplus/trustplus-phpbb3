<?php

namespace trustplus\ig\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

if (!defined('IN_PHPBB'))
  exit;

if (!class_exists('\TrustPlus\Client'))
  require_once(dirname(__FILE__) . '/../lib/TrustPlus/Client.php');

class main_listener implements EventSubscriberInterface {
  static public function getSubscribedEvents() {
    return array(
      'core.user_setup'                => 'load_language_on_setup',
      'core.ucp_register_data_after'   => 'on_register',
      'core.page_footer_after'         => 'on_footer',
    );
  }

  protected $config;
  protected $log;
  protected $user;
  protected $template;
  protected $request;

  public function __construct(\phpbb\config\config $config, \phpbb\log\log $log, \phpbb\user $user, \phpbb\template\template $template, \phpbb\request\request $request) {
    $this->config   = $config;
    $this->log      = $log;
    $this->user     = $user;
    $this->template = $template;
    $this->request  = $request;
  }

  public function load_language_on_setup($event) {
    $lang_set_ext = $event['lang_set_ext'];
    $lang_set_ext[] = array(
      'ext_name' => 'trustplus/ig',
      'lang_set' => 'common',
    );
    $event['lang_set_ext'] = $lang_set_ext;
  }

  public function on_footer($event) {
    global $template;

    $tai       = $this->config['trustplus_ig_tai'];
    $jsdomain  = $this->config['trustplus_ig_jsdomain'];
    $enableCAV = $this->config['trustplus_ig_enable_cav'];

    $template->assign_vars(array(
      'S_TRUSTPLUS_IG_ENABLE_CAV' => $enableCAV,
      'S_TRUSTPLUS_IG_TAI'        => $tai,
      'S_TRUSTPLUS_IG_JSDOMAIN'   => $jsdomain,
    ));
  }

  public function on_register($event) {
    // Are registration checks enabled?
    $registrationCheck = $this->config['trustplus_ig_registration_check'];
    if (!$registrationCheck)
      return;

    // No need to bother checking if there are already other errors.
    $error = $event['error'];
    if (count($event['error']) > 0)
      return;

    // Get config.
    $apiKey     = $this->config['trustplus_ig_api_key'];
    $endpoint   = $this->config['trustplus_ig_endpoint'];
    $sslVerify  = $this->config['trustplus_ig_ssl_verify'];
    $logAllowed = $this->config['trustplus_ig_log_allowed'];
    $enableCAV  = $this->config['trustplus_ig_enable_cav'];
    $approvalThreshold = $this->config['trustplus_ig_approval_threshold'];
    $debugDeny  = false;

    // Request properties.
    $email      = $event['data']['email'];
    $uid        = $this->user->data['user_id'];
    $ip         = $this->user->ip;
    $userAgent  = $this->request->header('User-Agent');
    $cavToken   = $this->request->variable('cav_token', '');
    if (!is_string($cavToken))
      $cavToken = '';

    // Call result variables.
    $permit = true;
    $result = array();
    $score  = null;

    // Call API.
    try {
      $tp_client = new \TrustPlus\Client();
      $tp_client->setAPIKey($apiKey);
      $tp_client->setEndpoint($endpoint);
      $tp_client->setVerifyPeer($sslVerify);

      // Assemble call arguments.
      $args = array(
        'email'       => $email,
        'ip'          => $ip,
        'user-agent'  => $userAgent,
      );

      if ($enableCAV)
        $args['client-token'] = $cavToken;

      // Make the call.
      $result = $tp_client->check($args);

      // Examine the result.
      if (is_array($result)) {
        if (isset($result['pass']) && $result['pass'] === false)
          $permit = false;

        if (isset($result['score']) && is_int($result['score']))
          $score = $result['score'];
      }

      else if (isset($result['fail-open']) && is_array($result['fail-open']))
        $this->log->add('critical', $uid, $ip, 'TRUSTPLUS_IG_FAIL_OPEN_LOG', $result['fail-open']['msg']);
    } catch (\Exception $e) {
      // fail open
      $this->log->add('critical', $uid, $ip, 'TRUSTPLUS_IG_CALL_FAILED_LOG', time(), array((string)$e));
    }

    // Consider what to do.
    $logPhrase = $logAllowed ? 'TRUSTPLUS_IG_REGISTRATION_ALLOWED_LOG' : '';
    if (!$permit) {
      $error[] = $this->user->lang['TRUSTPLUS_IG_REGISTRATION_DENIED'];
      $logPhrase = 'TRUSTPLUS_IG_REGISTRATION_DENIED_LOG';
    } else if ($approvalThreshold >= 0 && is_int($score) && $score >= $approvalThreshold) {
      $logPhrase = 'TRUSTPLUS_IG_REGISTRATION_MODERATED_LOG';
      if ($debugDeny)
        $error[] = $this->user->lang['TRUSTPLUS_IG_REGISTRATION_WOULD_MODERATE'];
      else {
        // make phpBB require admin activation by making it think the board is in
        // require admin activation mode, only for this request
        if ($this->config['email_enable'])
          $this->config['require_activation'] = USER_ACTIVATION_ADMIN;
      }
    } else {
      if ($debugDeny)
        $error[] = $this->user->lang['TRUSTPLUS_IG_REGISTRATION_WOULD_ALLOW'];
    }

    // Log the decision to the user log.
    $tagsMsg = '';
    if (isset($result['tags']))
      foreach ($result['tags'] as $k => $v)
        $tagsMsg .= ', tag ' . $k . ' (' . $v['score'] . ')';

    if ($logPhrase != '')
      $this->log->add('user', $uid, $ip, $logPhrase, time(), array($tagsMsg, $score, $ip, $email, $userAgent));

    // Return error.
    $event['error'] = $error;
  }
}
