Changelog
=========

1.0.2
-----

- Merge changes from upstream client library. Improves error reporting
  and resilience.

1.0.1
-----

- Successful and failed registrations can now be logged.
- Allow scores above a certain threshold to be flagged for manual approval.
- CAV support.

1.0.0
-----

- Initial release.
