<?php

namespace trustplus\ig\migrations;

class release_1_0_1 extends \phpbb\db\migration\migration {
  public function effectively_installed() {
    return isset($this->config['trustplus_ig_jsdomain']);
  }

  static public function depends_on() {
    return array('\trustplus\ig\migrations\release_1_0_0');
  }

  public function update_data() {
    return array(
      array('config.add', array('trustplus_ig_log_allowed', 0)),
      array('config.add', array('trustplus_ig_enable_cav', 1)),
      array('config.add', array('trustplus_ig_approval_threshold', -1)),
      array('config.add', array('trustplus_ig_tai', '')),
      array('config.add', array('trustplus_ig_jsdomain', 'cav.finalcdn.net')),
    );
  }
}
