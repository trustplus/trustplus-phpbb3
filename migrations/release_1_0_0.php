<?php

namespace trustplus\ig\migrations;

class release_1_0_0 extends \phpbb\db\migration\migration {
  public function effectively_installed() {
    return isset($this->config['trustplus_ig_registration_check']);
  }

  static public function depends_on() {
    return array('\phpbb\db\migration\data\v310\alpha2');
  }

  public function update_data() {
    return array(
      array('config.add', array('trustplus_ig_registration_check', 1)),
      array('config.add', array('trustplus_ig_api_key', '')),
      array('config.add', array('trustplus_ig_endpoint', 'https://api.trust.plus/check')),
      array('config.add', array('trustplus_ig_ssl_verify', 0)),

      array('module.add', array(
        'acp',
        'ACP_CAT_DOT_MODS',
        'ACP_TRUSTPLUS_IG_TITLE'
      )),
      array('module.add', array(
        'acp',
        'ACP_TRUSTPLUS_IG_TITLE',
        array(
          'module_basename'  => '\trustplus\ig\acp\main_module',
          'modes'        => array('settings'),
        ),
      )),
    );
  }
}
