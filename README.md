# Trust+ Integration Extension for phpBB 3.1+

## Download the Extention

  - [trustplus-phpbb3.zip](https://bitbucket.org/trustplus/trustplus-phpbb3/get/master.zip)
  - or clone: `git clone https://bitbucket.org/trustplus/trustplus-phpbb3 phpBB/ext/trustplus/ig`

## Installation Instructions

1. Copy the files into `phpBB/ext/trustplus/ig`, where `phpBB` is your phpBB directory. (The file `README.md` should be at `phpBB/ext/trustplus/ig/README.md`, not a subdirectory of it.)

2. Go to "ACP" > "Customise" > "Extensions" and enable the "Trust+ Integration Extension" extension.

3. Go to "Trust+ Integration Options" in the Extentions tab in the ACP, and specify your API key. You can find your API key by logging in to [https://trust.plus/](https://trust.plus/).

## Support

See [https://trust.plus/](https://trust.plus/).

## Licence

Licenced under the GPLv2 or later.
