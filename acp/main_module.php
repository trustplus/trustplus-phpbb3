<?php

namespace trustplus\ig\acp;

if (!defined('IN_PHPBB'))
  exit;

if (!class_exists('\TrustPlus\Client'))
  require_once(dirname(__FILE__) . '/../lib/TrustPlus/Client.php');

class main_module {
  var $u_action;

  function main($id, $mode) {
    global $db, $user, $auth, $template, $cache, $request;
    global $config, $phpbb_root_path, $phpbb_admin_path, $phpEx;

    $user->add_lang('acp/common');
    $this->tpl_name = 'ig_body';
    $this->page_title = $user->lang('ACP_TRUSTPLUS_IG_TITLE');
    add_form_key('trustplus/ig');

    if ($request->is_set_post('submit')) {
      if (!check_form_key('trustplus/ig'))
        trigger_error('FORM_INVALID');

      $api_key = $request->variable('trustplus_ig_api_key', '');
      $config->set('trustplus_ig_registration_check', $request->variable('trustplus_ig_registration_check', 0));
      $config->set('trustplus_ig_api_key', $api_key);
      $config->set('trustplus_ig_log_allowed', $request->variable('trustplus_ig_log_allowed', 0));
      $config->set('trustplus_ig_enable_cav', $request->variable('trustplus_ig_enable_cav', 0));
      $config->set('trustplus_ig_approval_threshold', $request->variable('trustplus_ig_approval_threshold', -1));

      $config->set('trustplus_ig_tai', \TrustPlus\Client::get_api_key_id($api_key));

      trigger_error($user->lang('ACP_TRUSTPLUS_IG_SETTINGS_SAVED') . adm_back_link($this->u_action));
    }

    $template->assign_vars(array(
      'U_ACTION'        => $this->u_action,
      'TRUSTPLUS_IG_REGISTRATION_CHECK'    => $config['trustplus_ig_registration_check'],
      'TRUSTPLUS_IG_API_KEY'               => $config['trustplus_ig_api_key'],
      'TRUSTPLUS_IG_LOG_ALLOWED'           => $config['trustplus_ig_log_allowed'],
      'TRUSTPLUS_IG_ENABLE_CAV'            => $config['trustplus_ig_enable_cav'],
      'TRUSTPLUS_IG_APPROVAL_THRESHOLD'    => $config['trustplus_ig_approval_threshold'],
    ));
  }
}
