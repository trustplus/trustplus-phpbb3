<?php

namespace trustplus\ig\acp;

class main_info {
  function module() {
    return array(
      'filename'  => '\trustplus\ig\acp\main_module',
      'title'    => 'ACP_IG_TITLE',
      'version'  => '1.0.0',
      'modes'    => array(
        'settings'  => array('title' => 'ACP_IG', 'auth' => 'ext_trustplus/ig && acl_a_board', 'cat' => array('ACP_IG_TITLE')),
      ),
    );
  }
}
