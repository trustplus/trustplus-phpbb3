<?php
// NNSC:trustplus-php/TrustPlus/Client.php 2016-04-13
/* Trust+ PHP Client Library
 * ============================================================================
 *
 * Requires PHP 5.3 or later.
 *
 * ----------------------------------------------------------------------------
 * © 2015 Trust+ <http://trust.plus/>
 * © 2015 Frederic Guillot
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace TrustPlus;

// Version check
if (!function_exists('array_replace'))
  die('The Trust+ client library requires PHP 5.3 or later.');

/* _RPCClient
 * ----------
 */
class RPCException extends \Exception {
  public $earr;

  public function __construct($earr=array()) {
    $this->earr = $earr;
  }

  public function __toString() {
    return \Exception::__toString() . "\nOther info: " . var_export($this->earr, true);
  }
};

class ConnectionFailureException extends RPCException {};
class ServerErrorException extends RPCException {};
class ClientErrorException extends RPCException {};
class AccessDeniedException extends ClientErrorException {};
class InvalidArgsException extends ClientErrorException {};
class InvalidMethodException extends ClientErrorException {};

class _RPCClient {
  private $_url; // endpoint URL
  private $_timeout; // in seconds
  public $_headers = array(
    'User-Agent: trustplus-php/1.1',
    'Content-Type: application/json-rpc',
    'Accept: application/json-rpc',
    'Connection: close',
  );
  private $_verifyPeer = true;

  public function __construct($url, $timeout, $username, $password) {
    $this->_url = $url;
    $this->_timeout = $timeout;

    if (!empty($username) || !empty($password))
      $this->_headers[] = 'Authorization: Basic ' . base64_encode($username . ':' . $password);
  }

  /* setVerifyPeer
   * -------------
   */
  public function setVerifyPeer($verifyPeer=true) {
    $this->_verifyPeer = $verifyPeer;
  }

  /* execute
   * -------
   */
  public function execute($method, $args=array()) {
    $req = array(
      'jsonrpc' => '2.0',
      'method'  => $method,
      'id'      => mt_rand(),
      'params'  => $args,
    );

    $ctx = stream_context_create(array(
      'http' => array(
        'method'           => 'POST',
        'protocol_version' => 1.0,
        'timeout'          => $this->_timeout,
        'max_redirects'    => 2,
        'header'           => implode("\r\n", $this->_headers),
        'content'          => json_encode($req),
        'ignore_errors'    => true,
      ),
      'ssl' => array(
        'verify_peer'         => $this->_verifyPeer,
        'verify_peer_name'    => $this->_verifyPeer,
        'allow_self_signed'   => !$this->_verifyPeer,
        'SNI_enabled'         => true,
        'disable_compression' => true,
      ),
    ));

    $stream = @fopen($this->_url, 'r', false, $ctx);
    if (!is_resource($stream))
      throw new ConnectionFailureException();

    // curlwrappers has a bug where stream_get_meta_data doesn't work until part of the
    // stream has been read. ...
    $initialChar = fread($stream, 1);
    if ($initialChar === FALSE)
      $initialChar = '';

    $code = self::_getHTTPStatusCode(stream_get_meta_data($stream)['wrapper_data']);
    if ($code >= 500 && $code < 600)
      throw new ServerErrorException();
    else if ($code == 401 || $code == 403)
      throw new AccessDeniedException();
    else if ($code >= 400 && $code < 500)
      throw new ClientErrorException();
    else if (!($code >= 200 && $code < 300))
      throw new RPCException('unknown error');

    $res = json_decode($initialChar . stream_get_contents($stream), true);
    if (!is_array($res))
      $res = array();

    if (isset($res['error']['code'])) {
      $error = $res['error'];
      switch ($error['code']) {
        case -32601:
          throw new InvalidMethodException($error['message']);
        case -32602:
          throw new InvalidArgsException($error['message']);
        default:
          throw new RPCException('RPC error: ' . $error['code'] . ': ' . $error['message']);
      }
    }

    return isset($res['result']) ? $res['result'] : null;
  }

  //
  private static function _getHTTPStatusCode($headers) {
    if (isset($headers['headers']))
      // There is some silly curlwrappers extension for PHP which does
      // everything wrong, like putting the headers in
      // ['wrapper_data']['headers'] rather than ['wrapper_data'].
      $headers = $headers['headers'];

    foreach ($headers as $h) {
      if (strpos($h, 'HTTP/') === 0) {
        $parts = explode(' ', substr($h, 5));
        if (count($parts) < 2)
          return 0;

        return (int)$parts[1];
      }
    }

    return 0;
  }
}

/* Client
 * ------
 */
class Client {
  const default_endpoint = 'https://api.trust.plus/check';
  private $_client;
  private $_endpoint;
  private $_apikey = '';
  private $_verifyPeer = false;
  private $_timeout = 30;

  /* constructor
   * -----------
   * Construct a new Trust+ API client object.
   */
  public function __construct() {
    $this->_endpoint = Client::default_endpoint;  
  }

  /* setEndpoint
   * -----------
   * Set the API endpoint. You shouldn't need to call this.
   */
  public function setEndpoint($endpoint) {
    $this->_endpoint = $endpoint;
    $this->_client = null;
  }

  /* setAPIKey
   * ---------
   * Set your API key. You must call this.
   */
  public function setAPIKey($apikey) {
    $this->_apikey = $apikey;
    $this->_client = null;
  }

  /* setVerifyPeer
   * -------------
   * This function can be used to enable SSL verification of the remote server.
   *
   * SSL verification is currently disabled by default, because the trust stores
   * of servers are too unpredictable to be relied upon. Your success with this
   * option may vary.
   */
  public function setVerifyPeer($verify=true) {
    $this->_verifyPeer = $verify;
    $this->_client = null;
  }

  /* setTimeout
   * ----------
   * Set the call timeout in seconds.
   */
  public function setTimeout($timeout) {
    $this->_timeout = $timeout;
    $this->_client = null;
  }

  /* check
   * -----
   * Make a check.
   *
   * The args parameter must be an array containing at least an 'ip' or 'email'
   * element, or both. See the Trust+ API documentation for more information.
   *
   * For example:
   *   $client->check([
   *     'ip' => '192.0.2.1',
   *     'email' => 'user@example.com',
   *   ]);
   *
   * Returns an array on success or throws an exception.
   * Example response:
   *
   *   [
   *     'pass': true,
   *     'score': 2,
   *   ]
   */
  public function check($args) {
    $this->_getClient();
    return $this->_client->execute('trustplus.Check', $args);
  }

  /* reportBan
   * ---------
   * Report a ban.
   *
   * The args parameter must be an array containing at least a 'reg-ip',
   * 'last-ip' or 'email' element. See the Trust+ API documentation for
   * more information.
   *
   * For example:
   *   $client->check([
   *     'reg-ip' => '192.0.2.1',
   *     'email'  => 'user@example.com',
   *   ]);
   *
   * Returns an (empty) array on success or throws an exception.
   */
  public function reportBan($args) {
    $this->_getClient();
    return $this->_client->execute('trustplus.ReportBan', $args);
  }

  /* getAPIKeyID
   * -----------
   * Returns the API key ID (a "tai_..." string) for the set API key.
   */
  public function getAPIKeyID() {
    return self::get_api_key_id($this->_apikey);
  }

  public static function get_api_key_id($apiKey) {
    return 'tai_' . rtrim(strtr(base64_encode(hash('sha256', $apiKey, true)), '+/', '-_'), '=');
  }

  //
  private function _getClient() {
    if ($this->_client)
      return;

    $this->_client = new _RPCClient($this->_endpoint, $this->_timeout, $this->_apikey, '');
    $this->_client->setVerifyPeer($this->_verifyPeer);

    return $this->_client;
  }
}
