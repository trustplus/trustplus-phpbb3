<?php

if (!defined('IN_PHPBB'))
  exit;

if (empty($lang) || !is_array($lang))
  $lang = array();

$lang = array_merge($lang, array(
  'ACP_IG_TITLE'       => 'Trust+ Integration',
  'ACP_TRUSTPLUS_IG_TITLE'       => 'Trust+ Integration',
  'ACP_IG'    => 'Settings',
  'ACP_TRUSTPLUS_IG_ENABLE_REGISTRATION_CHECK'      => 'Enable Registration Check',
  'ACP_TRUSTPLUS_IG_API_KEY'     => 'Trust+ API key',
  'ACP_TRUSTPLUS_IG_LOG_ALLOWED' => 'Log Allowed Registrations',
  'ACP_TRUSTPLUS_IG_ENABLE_CAV'  => 'Enable Client-Assisted Validation',
  'ACP_TRUSTPLUS_IG_APPROVAL_THRESHOLD' => 'Admin Approval Threshold',

  'ACP_TRUSTPLUS_IG_SETTINGS_SAVED'  => 'Settings have been saved successfully!',

  'ACP_TRUSTPLUS_IG_ENABLE_BAN_REPORTING' => 'Enable Ban Reporting',
  'ACP_TRUSTPLUS_IG_ENABLE_CAV' => 'Enable Client-Assisted Validation',
  'ACP_TRUSTPLUS_IG_MANUAL_APPROVAL_THRESHOLD' => 'Manual Approval Threshold',
  'ACP_TRUSTPLUS_IG_LOG_APPROVED_REGISTRATIONS' => 'Log Approved Registrations',

  'TRUSTPLUS_IG_CALL_FAILED_LOG' => 'Failed to make Trust+ check: %1$s',
  'TRUSTPLUS_IG_FAIL_OPEN_LOG' => 'Trust+ check failed open: %1$s',

  'TRUSTPLUS_IG_REGISTRATION_DENIED' => 'Registration with that e. mail address is not permitted. Please use a different e. mail address.',
  'TRUSTPLUS_IG_REGISTRATION_WOULD_ALLOW' => 'Registration would have been allowed (Trust+ debug).',
  'TRUSTPLUS_IG_REGISTRATION_WOULD_MODERATE' => 'Registration would have been moderated (Trust+ debug).',

  'TRUSTPLUS_IG_REGISTRATION_ALLOWED_LOG' => 'Trust+ allowed a registration: score %2$s, IP %3$s, email "%4$s", UA "%5$s"%1$s',
  'TRUSTPLUS_IG_REGISTRATION_DENIED_LOG' => 'Trust+ denied a registration: score %2$s, IP %3$s, email "%4$s", UA "%5$s"%1$s',
  'TRUSTPLUS_IG_REGISTRATION_MODERATED_LOG' => 'Trust+ flagged a registration for admin approval: %2$s, IP %3$s, email "%4$s", UA "%5$s"%1$s',
));
